<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Portfolio - Michiel Gulickx</title>
</head>

<body>
<div id="header">
    <section class="header-logo col col-xxs-12">
        <h1>
            <a href="<?php print $front_page;?>">
                <img id="templogo" src="<?php print $base_path?>/<?php print $directory;?>/images/logo.png" alt="<?php print $site_name;?>" height="47" width="217" />
            </a>
        </h1>
    </section>
</div>

<div id="navigation">
    <?php if ($main_menu): ?>
        <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu'))); ?>
    <?php endif; ?>
</div>


<div id="bigwrapper">
    <div id="wrapper">
        <div id="content" class="col-xxs-12 col-md-7">
            <?php print render($title_prefix); ?>
            <?php if ($title): ?><h1><?php print $title; ?></h1><?php endif; ?>
            <?php print render($title_suffix); ?>
            <?php print render($messages); ?>
            <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
            <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
            <?php print render($page['content']); ?>
        </div>


    </div>
</div>

<div id="footer">
    <?php if ($page['footer']): ?>
        <?php print render($page['footer']); ?>
    <?php endif; ?>
</div>

</body>
</html>
